#!/usr/bin/env bash

set -euo pipefail
set -x

script_dir=$(dirname $(readlink -f $0))

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

checkout()
{
    if [ -d node ]; then
        return
    fi
    git clone https://github.com/nodejs/node
    pushd node
    git reset --hard $version
    git log -n1

    # gyp generates a variable named PRODUCT_DIR_ABS. it uses '\' to split path.
    # it is used to generate some macro values for openssl, like MODULESDIR
    #
    # Alas, if your path contains a folder starting with numbers (\456)
    # (it happens with gitlab ci, if your runner token starts with numbers)
    # the macro will be expanded to C:\...\456..\..., which is evaluated
    # as an escape sequence.
    #
    # So we hack this directly by changing gyp file with correct path
    current_path_with_slash="$(cygpath -m "$(pwd)")"
    sed -e "s#<(PRODUCT_DIR_ABS)#$current_path_with_slash#" -i deps/openssl/openssl.gyp
    popd
}

get_python()
{
    # for some reason, nodejs build does not like arm64 python (gyp?)
    # error when build x64 version
    # error MSB4126: The specified solution configuration "Release|x64" is invalid. Please specify a valid solution configuration using the Configuration and Platform properties (e.g. MSBuild.exe Solution.sln /p:Configuration=Debug /p:Platform="Any CPU") or leave those properties blank to use the default solution configuration. [C:\work\recipes\nodejs-testing\node\node. sln]

    if [ -d python ]; then
        return
    fi
    mkdir python
    pushd python
    wget https://www.nuget.org/api/v2/package/python/3.10.6 -O python.zip
    unzip python.zip
    popd
}

build()
{
    pushd node
    build_arch=$1; shift

    get_python

    cat > do_build.bat << EOF
call vcvarsall.bat /clean_env || exit 1
set PATH=%CD%/python/tools;%PATH% || exit 1
which python.exe || exit 1
call vcbuild.bat $build_arch openssl-no-asm vs2019 || exit 1
EOF

    cmd /c do_build.bat

    file out/Release/node.exe
    popd
}

test()
{
    pushd node
    build_arch=$1; shift
    test_log_file=$1; shift

    echo "remove some tests"
    # failure in CI, but works in official one and when trying interactively
    rm -f test/parallel/test-http2-misbehaving-multiplex.js

    echo "run tests"
    rm -rf test.tap
    python tools/test.py \
        --mode=release --flaky-tests=skip -p tap \
        -j 1 \
        --logfile test.tap default pummel || true
    [ -f test.tap ] || die "missing test results"

    # failed tests are marked with "not ok", good tests with "ok"
    # remove test number since it's random
    cat test.tap |
        sed -e 's/ [0-9]\+ / /' |
        (grep "^not ok" || true) |
        sort > $test_log_file

    echo "failed tests for $build_arch:"
    cat $test_log_file
    popd
}

kill_node()
{
    cmd /c 'taskkill.exe /IM node.exe /F' || true
}

tmp=$(mktemp -d)
trap "rm -rf $tmp; kill_node" EXIT
checkout
kill_node
build x64
test x64 $tmp/test-x64.log
kill_node
git -C node clean -ffdx # clean everything
build arm64
test arm64 $tmp/test-arm64.log
kill_node
different=0
diff $tmp/test-x64.log $tmp/test-arm64.log || different=1

if [ $different -eq 0 ]; then
    echo "no failure found for a specific platform"
    exit 0
else
    echo "some tests only failed on one platform"
    exit 1
fi
